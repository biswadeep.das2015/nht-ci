# Setting up Jenkins
   - Install the required plugins like Docker, nodejs
   - Create a project in Jenkins
   - Add the repo from which the webhook will be set up
   - Add the gitlab credentials for authetication
   - Add the docker credentials for authentication 

# Adding hooks
   - Add the hooks in gitlab 
   - Make a dummy push to check the hooks   

# Runners in Gitlab

   - Create runners in gitlab
   - Register the runner from the VM
   - Make a dummy push to check if runner is working

# Problem faced
   - The runner was shared initially hence it was failing before
   - The VM didn't have maven installed before hence the runner was failing.   